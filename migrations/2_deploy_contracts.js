const GGEasyToken = artifacts.require('GGEasyToken')

module.exports = async function(deployer, network, accounts) {
  // Deploy Dapp Token
  await deployer.deploy(GGEasyToken)
  const ggEasyToken = await GGEasyToken.deployed()

  // Transfer all tokens to TokenFarm (1 million)
  await ggEasyToken.transfer('0x98EEaEd33fc0d774A46f9b63208d51A74fcCa3fF', '1000000000000000000000000')
}